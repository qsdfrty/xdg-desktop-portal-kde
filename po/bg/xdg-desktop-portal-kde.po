# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the xdg-desktop-portal-kde package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: xdg-desktop-portal-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-24 01:57+0000\n"
"PO-Revision-Date: 2023-08-29 14:47+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: \n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Минчо Кондарев"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mkondarev@yahoo.de"

#: src/accessdialog.cpp:22
#, kde-format
msgid "Request device access"
msgstr "Заявяване достъп до устройство"

#: src/appchooser.cpp:92
#, kde-format
msgctxt "count of files to open"
msgid "%1 files"
msgstr "%1 файла"

#: src/appchooserdialog.cpp:39
#, kde-format
msgctxt "@title:window"
msgid "Choose Application"
msgstr "Избор на приложение"

#: src/appchooserdialog.cpp:42
#, kde-kuit-format
msgctxt "@info"
msgid "Choose an application to open <filename>%1</filename>"
msgstr "Изберете приложение за отваряне на <filename>%1</filename>"

#: src/AppChooserDialog.qml:31
#, kde-format
msgctxt "@option:check %1 is description of a file type, like 'PNG image'"
msgid "Always open %1 files with the chosen app"
msgstr "Файлове  от тип %1 винаги да се отварят с избраното приложение"

#: src/AppChooserDialog.qml:64
#, kde-format
msgid "Show All Installed Applications"
msgstr "Показване на всички инсталирани приложения"

#: src/AppChooserDialog.qml:170
#, kde-format
msgid "Default app for this file type"
msgstr "Стандартно приложение за този тип файлове"

#: src/AppChooserDialog.qml:171
#, kde-format
msgctxt "@info:whatsthis"
msgid "Last used app for this file type"
msgstr "Последно използвано приложение за този тип файлове"

#: src/AppChooserDialog.qml:190
#, kde-format
msgid "No matches"
msgstr "Няма съвпадения"

#: src/AppChooserDialog.qml:190
#, kde-kuit-format
msgctxt "@info"
msgid "No installed applications can open <filename>%1</filename>"
msgstr "Няма инсталирано приложение за отваряне на <filename>%1</filename>"

#: src/AppChooserDialog.qml:194
#, kde-format
msgctxt "Find some more apps that can open this content using the Discover app"
msgid "Find More in Discover"
msgstr "Търсене на още приложения в Discover"

#: src/AppChooserDialog.qml:209
#, kde-kuit-format
msgctxt "@info"
msgid "Don't see the right app? Find more in <link>Discover</link>."
msgstr ""
"Не намирате подходящо приложение? Потърсете за още приложения в "
"<link>Discover</link>."

#: src/background.cpp:103
#, kde-format
msgid "Background Activity"
msgstr "Дейност в заден план"

#: src/background.cpp:104
#, kde-format
msgctxt "@info %1 is the name of an application"
msgid ""
"%1 wants to remain running when it has no visible windows. If you forbid "
"this, the application will quit when its last window is closed."
msgstr ""
"%1 иска да продължи да работи, когато няма видими прозорци. Ако забраните "
"това, приложението ще се прекрати, когато последният му прозорец бъде "
"затворен."

#: src/background.cpp:106 src/background.cpp:147
#, kde-format
msgctxt "@action:button Allow the app to keep running with no open windows"
msgid "Allow"
msgstr "Разрешаване"

#: src/background.cpp:107
#, kde-format
msgctxt ""
"@action:button Don't allow the app to keep running without any open windows"
msgid "Forbid"
msgstr "Забраняване"

#: src/background.cpp:141
#, kde-format
msgctxt ""
"@title title of a dialog to confirm whether to allow an app to remain "
"running with no visible windows"
msgid "Background App Usage"
msgstr "Използване на приложения в заден план"

#: src/background.cpp:143
#, kde-format
msgctxt "%1 is the name of an application"
msgid ""
"Note that this will force %1 to quit when its last window is closed. This "
"could cause data loss if the application has any unsaved changes when it "
"happens."
msgstr ""
"Обърнете внимание, че това ще принуди %1 да приключи, когато последният му "
"прозорец бъде затворен. Това може да доведе до загуба на данни, ако "
"приложението има незаписани промени."

#: src/background.cpp:148
#, kde-format
msgctxt ""
"@action:button Don't allow the app to keep running without any open windows"
msgid "Forbid Anyway"
msgstr "Забраняване въпреки това"

#: src/dynamiclauncher.cpp:76
#, kde-format
msgctxt "@title"
msgid "Add Web Application…"
msgstr "Добавяне на уеб приложение…"

#: src/dynamiclauncher.cpp:81
#, kde-format
msgctxt "@title"
msgid "Add Application…"
msgstr "Добавяне на приложение…"

#: src/DynamicLauncherDialog.qml:72
#, kde-format
msgctxt "@label name of a launcher/application"
msgid "Name"
msgstr "Име"

#: src/DynamicLauncherDialog.qml:91
#, kde-format
msgctxt "@action edit launcher name/icon"
msgid "Edit Info…"
msgstr "Редактиране на информация…"

#: src/DynamicLauncherDialog.qml:97
#, kde-format
msgctxt "@action accept dialog and create launcher"
msgid "Accept"
msgstr "Приемане"

#: src/DynamicLauncherDialog.qml:102
#, kde-format
msgctxt "@action"
msgid "Cancel"
msgstr "Прекъсване"

#: src/filechooser.cpp:432
#, kde-format
msgid "Open"
msgstr "Отваряне"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:101
#, kde-format
msgid "Select Folder"
msgstr "Избор на папка"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:104
#, kde-format
msgid "Open File"
msgstr "Отваряне на файл"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:106
#, kde-format
msgid "Save File"
msgstr "Запазване на файл"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:17
#, kde-format
msgid "Create New Folder"
msgstr "Създаване на нова папка"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:26
#, kde-format
msgid "Create new folder in %1"
msgstr "Създаване на нова папка в %1"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:33
#, kde-format
msgid "Folder name"
msgstr "Име на папка"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:41
#, kde-format
msgid "OK"
msgstr "Добре"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:52
#, kde-format
msgid "Cancel"
msgstr "Отказ"

#: src/kirigami-filepicker/declarative/FilePicker.qml:101
#, kde-format
msgid "File name"
msgstr "Име на файл"

#: src/kirigami-filepicker/declarative/FilePicker.qml:116
#, kde-format
msgid "Select"
msgstr "Избиране"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:57
#, kde-format
msgid "Create Folder"
msgstr "Създаване на папка"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:66
#, kde-format
msgid "Filter Filetype"
msgstr "Филтриране на файлов тип"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:71
#, kde-format
msgid "Show Hidden Files"
msgstr "Показване на скритите файлове"

#: src/outputsmodel.cpp:19
#, kde-format
msgid "Full Workspace"
msgstr "Цяло работно пространство"

#: src/outputsmodel.cpp:24
#, kde-format
msgid "New Virtual Output"
msgstr "Нов виртуален изход"

#: src/outputsmodel.cpp:28
#, kde-format
msgid "Rectangular Region"
msgstr "Правоъгълна област"

#: src/outputsmodel.cpp:45
#, kde-format
msgid "Laptop screen"
msgstr "Екран на лаптоп"

#: src/region-select/RegionSelectOverlay.qml:142
#, kde-format
msgid "Start streaming:"
msgstr "Стрийминг:"

#: src/region-select/RegionSelectOverlay.qml:146
#, kde-format
msgid "Clear selection:"
msgstr "Изчистване на избора:"

#: src/region-select/RegionSelectOverlay.qml:150
#: src/region-select/RegionSelectOverlay.qml:193
#, kde-format
msgid "Cancel:"
msgstr "Отказ:"

#: src/region-select/RegionSelectOverlay.qml:156
#, kde-format
msgctxt "Mouse action"
msgid "Release left-click"
msgstr "Освободете с щракване с левия бутон на мишката"

#: src/region-select/RegionSelectOverlay.qml:160
#, kde-format
msgctxt "Mouse action"
msgid "Right-click"
msgstr "Кликване с десен бутон"

#: src/region-select/RegionSelectOverlay.qml:164
#: src/region-select/RegionSelectOverlay.qml:203
#, kde-format
msgctxt "Keyboard action"
msgid "Escape"
msgstr "Напускане"

#: src/region-select/RegionSelectOverlay.qml:189
#, kde-format
msgid "Create selection:"
msgstr "Създаване на селекция:"

#: src/region-select/RegionSelectOverlay.qml:199
#, kde-format
msgctxt "Mouse action"
msgid "Left-click and drag"
msgstr "Щракване с ляв бутон и завличане"

#: src/remotedesktop.cpp:129
#, kde-format
msgctxt "title of notification about input systems taken over"
msgid "Remote control session started"
msgstr "Отдалечената сесия започна"

#: src/remotedesktopdialog.cpp:23
#, kde-format
msgctxt "Title of the dialog that requests remote input privileges"
msgid "Remote control requested"
msgstr "Заявен е отдалечен контрол"

#: src/remotedesktopdialog.cpp:32
#, kde-format
msgctxt "Unordered list with privileges granted to an external process"
msgid "Requested access to:\n"
msgstr "Заявен достъп до:\n"

#: src/remotedesktopdialog.cpp:34
#, kde-format
msgctxt ""
"Unordered list with privileges granted to an external process, included the "
"app's name"
msgid "%1 requested access to remotely control:\n"
msgstr "%1 поиска достъп за отдалечен контрол:\n"

#: src/remotedesktopdialog.cpp:37
#, kde-format
msgctxt "Will allow the app to see what's on the outputs, in markdown"
msgid " - Screens\n"
msgstr " - Екрани\n"

#: src/remotedesktopdialog.cpp:40
#, kde-format
msgctxt "Will allow the app to send input events, in markdown"
msgid " - Input devices\n"
msgstr " - Входни устройства\n"

#: src/RemoteDesktopDialog.qml:25 src/ScreenChooserDialog.qml:132
#: src/UserInfoDialog.qml:41
#, kde-format
msgid "Share"
msgstr "Споделяне"

#: src/screencast.cpp:322
#, kde-format
msgctxt "Do not disturb mode is enabled because..."
msgid "Screen sharing in progress"
msgstr "В процес на споделяне на екрана"

#: src/screenchooserdialog.cpp:143
#, kde-format
msgid "Screen Sharing"
msgstr "Споделяне на екрана"

#: src/screenchooserdialog.cpp:173
#, kde-format
msgid "Choose what to share with the requesting application:"
msgstr "Изберете какво да споделите със заявяващото приложение:"

#: src/screenchooserdialog.cpp:175
#, kde-format
msgid "Choose what to share with %1:"
msgstr "Изберете какво да споделяте с %1:"

#: src/screenchooserdialog.cpp:183
#, kde-format
msgid "Share this screen with the requesting application?"
msgstr "Да се сподели ли екрана със заявяващото приложение?"

#: src/screenchooserdialog.cpp:185
#, kde-format
msgid "Share this screen with %1?"
msgstr "Да се сподели ли екрана с %1?"

#: src/screenchooserdialog.cpp:190
#, kde-format
msgid "Choose screens to share with the requesting application:"
msgstr "Избиране на екран за споделяне със заявяващото приложение:"

#: src/screenchooserdialog.cpp:192
#, kde-format
msgid "Choose screens to share with %1:"
msgstr "Избиране на екран за споделяне с %1:"

#: src/screenchooserdialog.cpp:196
#, kde-format
msgid "Choose which screen to share with the requesting application:"
msgstr "Избиране на екран да споделяне със заявяващото приложение:"

#: src/screenchooserdialog.cpp:198
#, kde-format
msgid "Choose which screen to share with %1:"
msgstr "Избиране на екран за споделяне с %1:"

#: src/screenchooserdialog.cpp:208
#, kde-format
msgid "Share this window with the requesting application?"
msgstr "Да се сподели този прозорец със заявяващото приложение?"

#: src/screenchooserdialog.cpp:210
#, kde-format
msgid "Share this window with %1?"
msgstr "Да се сподели ли този прозорец с %1?"

#: src/screenchooserdialog.cpp:215
#, kde-format
msgid "Choose windows to share with the requesting application:"
msgstr "Избиране на прозорец за споделяне със заявяващото приложение:"

#: src/screenchooserdialog.cpp:217
#, kde-format
msgid "Choose windows to share with %1:"
msgstr "Избиране на прозорец за споделяне с %1:"

#: src/screenchooserdialog.cpp:221
#, kde-format
msgid "Choose which window to share with the requesting application:"
msgstr "Избиране на прозорец за споделяне със заявяващото приложение:"

#: src/screenchooserdialog.cpp:223
#, kde-format
msgid "Choose which window to share with %1:"
msgstr "Избиране на прозорец за споделяне с %1:"

#: src/ScreenChooserDialog.qml:36
#, kde-format
msgid "Screens"
msgstr "Екрани"

#: src/ScreenChooserDialog.qml:39
#, kde-format
msgid "Windows"
msgstr "Прозорци"

#: src/ScreenChooserDialog.qml:126
#, kde-format
msgid "Allow restoring on future sessions"
msgstr "Позволяване на възстановяване на бъдещи сесии"

#: src/screenshotdialog.cpp:115
#, kde-format
msgid "Full Screen"
msgstr "Цял екран"

#: src/screenshotdialog.cpp:116
#, kde-format
msgid "Current Screen"
msgstr "Текущ екран"

#: src/screenshotdialog.cpp:117
#, kde-format
msgid "Active Window"
msgstr "Текущ прозорец"

#: src/ScreenshotDialog.qml:23
#, kde-format
msgid "Request Screenshot"
msgstr "Заявяване на снимка на екрана"

#: src/ScreenshotDialog.qml:30
#, kde-format
msgid "Capture Mode"
msgstr "Режим на прихващане"

#: src/ScreenshotDialog.qml:34
#, kde-format
msgid "Area:"
msgstr "Област:"

#: src/ScreenshotDialog.qml:39
#, kde-format
msgid "Delay:"
msgstr "Изчакване:"

#: src/ScreenshotDialog.qml:44
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 секунда"
msgstr[1] "%1 секунди"

#: src/ScreenshotDialog.qml:52
#, kde-format
msgid "Content Options"
msgstr "Настройки на съдържанието"

#: src/ScreenshotDialog.qml:56
#, kde-format
msgid "Include cursor pointer"
msgstr "Включване курсора на мишката"

#: src/ScreenshotDialog.qml:61
#, kde-format
msgid "Include window borders"
msgstr "Включване на рамката на прозореца"

#: src/ScreenshotDialog.qml:74
#, kde-format
msgid "Save"
msgstr "Запазване"

#: src/ScreenshotDialog.qml:84
#, kde-format
msgid "Take"
msgstr "Поемане на контрол"

#: src/session.cpp:169
#, kde-format
msgctxt "@action:inmenu stops screen/window sharing"
msgid "End"
msgstr "Край"

#: src/session.cpp:255
#, kde-format
msgctxt ""
"SNI title that indicates there's a process remotely controlling the system"
msgid "Remote Desktop"
msgstr "Отдалечен работен плот"

#: src/session.cpp:271
#, kde-format
msgctxt "%1 number of screens, %2 the app that receives them"
msgid "Sharing contents to %2"
msgid_plural "%1 video streams to %2"
msgstr[0] "Споделяне на съдържание с %2"
msgstr[1] "%1 видео потоци към %2"

#: src/session.cpp:275
#, kde-format
msgctxt ""
"SNI title that indicates there's a process seeing our windows or screens"
msgid "Screen casting"
msgstr "Споделяне на екрана"

#: src/session.cpp:441
#, kde-format
msgid ", "
msgstr ", "

#: src/userinfodialog.cpp:32
#, kde-format
msgid "Share Information"
msgstr "Споделяне на информация"

#: src/userinfodialog.cpp:33
#, kde-format
msgid "Share your personal information with the requesting application?"
msgstr "Да се сподели ли вашата лична информация със заявяващото приложение?"

#: src/waylandintegration.cpp:260 src/waylandintegration.cpp:315
#, kde-format
msgid "Failed to start screencasting"
msgstr "Неуспешно стартиране на видеозапис на екрана"

#: src/waylandintegration.cpp:619
#, kde-format
msgid "Remote desktop"
msgstr "Отдалечен работен плот"

#~ msgid ""
#~ "Laptop screen\n"
#~ "Model: %1"
#~ msgstr ""
#~ "Екран на лаптоп\n"
#~ "Модел: %1"

#~ msgid ""
#~ "Manufacturer: %1\n"
#~ "Model: %2"
#~ msgstr ""
#~ "Производител: %1\n"
#~ "Модел: %2"
