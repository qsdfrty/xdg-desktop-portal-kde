# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# giovanni <g.sora@tiscali.it>, 2018, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-24 01:57+0000\n"
"PO-Revision-Date: 2023-08-24 10:30+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: src/accessdialog.cpp:22
#, kde-format
msgid "Request device access"
msgstr "Require accesso de dispositivo"

#: src/appchooser.cpp:92
#, kde-format
msgctxt "count of files to open"
msgid "%1 files"
msgstr "%1 files"

#: src/appchooserdialog.cpp:39
#, kde-format
msgctxt "@title:window"
msgid "Choose Application"
msgstr "Selige application "

#: src/appchooserdialog.cpp:42
#, kde-kuit-format
msgctxt "@info"
msgid "Choose an application to open <filename>%1</filename>"
msgstr "Selige un application a aperir <filename>%1</filename>"

#: src/AppChooserDialog.qml:31
#, kde-format
msgctxt "@option:check %1 is description of a file type, like 'PNG image'"
msgid "Always open %1 files with the chosen app"
msgstr "Sempre aperi %1 files con le app seligite"

#: src/AppChooserDialog.qml:64
#, kde-format
msgid "Show All Installed Applications"
msgstr "Monstra omne  applicationes installate "

#: src/AppChooserDialog.qml:170
#, kde-format
msgid "Default app for this file type"
msgstr "App predefinite per iste typo de file"

#: src/AppChooserDialog.qml:171
#, kde-format
msgctxt "@info:whatsthis"
msgid "Last used app for this file type"
msgstr "Ultime app usate per iste typo de file"

#: src/AppChooserDialog.qml:190
#, kde-format
msgid "No matches"
msgstr "Nulle correspondentias"

#: src/AppChooserDialog.qml:190
#, kde-kuit-format
msgctxt "@info"
msgid "No installed applications can open <filename>%1</filename>"
msgstr "Nulle applicationes installate pote aperir <filename>%1</filename>"

#: src/AppChooserDialog.qml:194
#, kde-format
msgctxt "Find some more apps that can open this content using the Discover app"
msgid "Find More in Discover"
msgstr "Trova plus in Discover"

#: src/AppChooserDialog.qml:209
#, kde-kuit-format
msgctxt "@info"
msgid "Don't see the right app? Find more in <link>Discover</link>."
msgstr "Non vide le correcte app? Trova plus in <link>Discover</link>"

#: src/background.cpp:103
#, kde-format
msgid "Background Activity"
msgstr "Activitate de Fundo"

#: src/background.cpp:104
#, kde-format
msgctxt "@info %1 is the name of an application"
msgid ""
"%1 wants to remain running when it has no visible windows. If you forbid "
"this, the application will quit when its last window is closed."
msgstr ""
"%1 ole remaner executante quando il non ha fenestras visibile. Si tu prohibe "
"isto, le application abandonara quando su ultime fenestra es claudite."

#: src/background.cpp:106 src/background.cpp:147
#, kde-format
msgctxt "@action:button Allow the app to keep running with no open windows"
msgid "Allow"
msgstr "Permitte"

#: src/background.cpp:107
#, kde-format
msgctxt ""
"@action:button Don't allow the app to keep running without any open windows"
msgid "Forbid"
msgstr "Prohibe"

#: src/background.cpp:141
#, kde-format
msgctxt ""
"@title title of a dialog to confirm whether to allow an app to remain "
"running with no visible windows"
msgid "Background App Usage"
msgstr "Uso de App de Fundo"

#: src/background.cpp:143
#, kde-format
msgctxt "%1 is the name of an application"
msgid ""
"Note that this will force %1 to quit when its last window is closed. This "
"could cause data loss if the application has any unsaved changes when it "
"happens."
msgstr ""
"Nota que isto fortiara %1  abandonar quando su ultime fenestra es claudite. "
"Isto poterea causar perdita de datos si le application ha ulle "
"modificationes non salveguardte quando illo ocurre."

#: src/background.cpp:148
#, kde-format
msgctxt ""
"@action:button Don't allow the app to keep running without any open windows"
msgid "Forbid Anyway"
msgstr "Prohibe de omne modo"

#: src/dynamiclauncher.cpp:76
#, kde-format
msgctxt "@title"
msgid "Add Web Application…"
msgstr "Adde  application de web"

#: src/dynamiclauncher.cpp:81
#, kde-format
msgctxt "@title"
msgid "Add Application…"
msgstr "Adde Application..."

#: src/DynamicLauncherDialog.qml:72
#, kde-format
msgctxt "@label name of a launcher/application"
msgid "Name"
msgstr "Nomine"

#: src/DynamicLauncherDialog.qml:91
#, kde-format
msgctxt "@action edit launcher name/icon"
msgid "Edit Info…"
msgstr "Modifica Info..."

#: src/DynamicLauncherDialog.qml:97
#, kde-format
msgctxt "@action accept dialog and create launcher"
msgid "Accept"
msgstr "Accepta"

#: src/DynamicLauncherDialog.qml:102
#, kde-format
msgctxt "@action"
msgid "Cancel"
msgstr "Cancella"

#: src/filechooser.cpp:432
#, kde-format
msgid "Open"
msgstr "Aperi"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:101
#, kde-format
msgid "Select Folder"
msgstr "Selige dossier"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:104
#, kde-format
msgid "Open File"
msgstr "Aperi file"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:106
#, kde-format
msgid "Save File"
msgstr "Salveguarda file"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:17
#, kde-format
msgid "Create New Folder"
msgstr "Crea nove dossier"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:26
#, kde-format
msgid "Create new folder in %1"
msgstr "Crea nove dossier in %1"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:33
#, kde-format
msgid "Folder name"
msgstr "Nomine de dossier"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:41
#, kde-format
msgid "OK"
msgstr "OK"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:52
#, kde-format
msgid "Cancel"
msgstr "Cancella"

#: src/kirigami-filepicker/declarative/FilePicker.qml:101
#, kde-format
msgid "File name"
msgstr "Nomine de file"

#: src/kirigami-filepicker/declarative/FilePicker.qml:116
#, kde-format
msgid "Select"
msgstr "Selige"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:57
#, kde-format
msgid "Create Folder"
msgstr "Crea dossier"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:66
#, kde-format
msgid "Filter Filetype"
msgstr "Filtra typo de file"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:71
#, kde-format
msgid "Show Hidden Files"
msgstr "Monstra files celate"

#: src/outputsmodel.cpp:19
#, kde-format
msgid "Full Workspace"
msgstr "Spatio de labor Plen"

#: src/outputsmodel.cpp:24
#, kde-format
msgid "New Virtual Output"
msgstr "Nove Sortita Virtual"

#: src/outputsmodel.cpp:28
#, kde-format
msgid "Rectangular Region"
msgstr "Region rectangular"

#: src/outputsmodel.cpp:45
#, kde-format
msgid "Laptop screen"
msgstr "Schermo de Laptop"

#: src/region-select/RegionSelectOverlay.qml:142
#, kde-format
msgid "Start streaming:"
msgstr "Initia streaming:"

#: src/region-select/RegionSelectOverlay.qml:146
#, kde-format
msgid "Clear selection:"
msgstr "Netta selection:"

#: src/region-select/RegionSelectOverlay.qml:150
#: src/region-select/RegionSelectOverlay.qml:193
#, kde-format
msgid "Cancel:"
msgstr "Cancella:"

#: src/region-select/RegionSelectOverlay.qml:156
#, kde-format
msgctxt "Mouse action"
msgid "Release left-click"
msgstr "Relaxa clicca-sinistre"

#: src/region-select/RegionSelectOverlay.qml:160
#, kde-format
msgctxt "Mouse action"
msgid "Right-click"
msgstr "Pulsa a dextera:"

#: src/region-select/RegionSelectOverlay.qml:164
#: src/region-select/RegionSelectOverlay.qml:203
#, kde-format
msgctxt "Keyboard action"
msgid "Escape"
msgstr "Escape"

#: src/region-select/RegionSelectOverlay.qml:189
#, kde-format
msgid "Create selection:"
msgstr "Crea selection:"

#: src/region-select/RegionSelectOverlay.qml:199
#, kde-format
msgctxt "Mouse action"
msgid "Left-click and drag"
msgstr "Clicca a sinistra e trahe"

#: src/remotedesktop.cpp:129
#, kde-format
msgctxt "title of notification about input systems taken over"
msgid "Remote control session started"
msgstr "Session de controlo remote initiava"

#: src/remotedesktopdialog.cpp:23
#, kde-format
msgctxt "Title of the dialog that requests remote input privileges"
msgid "Remote control requested"
msgstr "Controlo remote requirite"

#: src/remotedesktopdialog.cpp:32
#, kde-format
msgctxt "Unordered list with privileges granted to an external process"
msgid "Requested access to:\n"
msgstr "Requireva accesso a:\n"

#: src/remotedesktopdialog.cpp:34
#, kde-format
msgctxt ""
"Unordered list with privileges granted to an external process, included the "
"app's name"
msgid "%1 requested access to remotely control:\n"
msgstr "%1 requireva accesso a controlo remotemente:\n"

#: src/remotedesktopdialog.cpp:37
#, kde-format
msgctxt "Will allow the app to see what's on the outputs, in markdown"
msgid " - Screens\n"
msgstr " - Schermos\n"

#: src/remotedesktopdialog.cpp:40
#, kde-format
msgctxt "Will allow the app to send input events, in markdown"
msgid " - Input devices\n"
msgstr "- Dispositivos de entrata\n"

#: src/RemoteDesktopDialog.qml:25 src/ScreenChooserDialog.qml:132
#: src/UserInfoDialog.qml:41
#, kde-format
msgid "Share"
msgstr "Comparti"

#: src/screencast.cpp:322
#, kde-format
msgctxt "Do not disturb mode is enabled because..."
msgid "Screen sharing in progress"
msgstr "Compartir de schermo in progresso"

#: src/screenchooserdialog.cpp:143
#, kde-format
msgid "Screen Sharing"
msgstr "Compartir de schermo"

#: src/screenchooserdialog.cpp:173
#, kde-format
msgid "Choose what to share with the requesting application:"
msgstr "Selige que compartir con le application requiriente:"

#: src/screenchooserdialog.cpp:175
#, kde-format
msgid "Choose what to share with %1:"
msgstr "Selige que compartir con %1:"

#: src/screenchooserdialog.cpp:183
#, kde-format
msgid "Share this screen with the requesting application?"
msgstr "Comparti iste schermo con le application requiriente"

#: src/screenchooserdialog.cpp:185
#, kde-format
msgid "Share this screen with %1?"
msgstr "Comparti iste schermo con %1?"

#: src/screenchooserdialog.cpp:190
#, kde-format
msgid "Choose screens to share with the requesting application:"
msgstr "Selige schermo de compartir con le application requiriente"

#: src/screenchooserdialog.cpp:192
#, kde-format
msgid "Choose screens to share with %1:"
msgstr "Selige schermos a compartir con %1:"

#: src/screenchooserdialog.cpp:196
#, kde-format
msgid "Choose which screen to share with the requesting application:"
msgstr "Selige qual schermo a compartir con le application requiriente:"

#: src/screenchooserdialog.cpp:198
#, kde-format
msgid "Choose which screen to share with %1:"
msgstr "Selige le schermo con le qual de compartir  %1:"

#: src/screenchooserdialog.cpp:208
#, kde-format
msgid "Share this window with the requesting application?"
msgstr "Comparti iste fenestra con le application requiriente"

#: src/screenchooserdialog.cpp:210
#, kde-format
msgid "Share this window with %1?"
msgstr "Comparti iste fenestra con %1?"

#: src/screenchooserdialog.cpp:215
#, kde-format
msgid "Choose windows to share with the requesting application:"
msgstr "Selige fenestras a compartir con le application requiriente:"

#: src/screenchooserdialog.cpp:217
#, kde-format
msgid "Choose windows to share with %1:"
msgstr "Selige fenestras a compartir con %1"

#: src/screenchooserdialog.cpp:221
#, kde-format
msgid "Choose which window to share with the requesting application:"
msgstr "Selige qual fenestra compartir con le application requiriente:"

#: src/screenchooserdialog.cpp:223
#, kde-format
msgid "Choose which window to share with %1:"
msgstr "Selige qual fenestra a compartir con %1:"

#: src/ScreenChooserDialog.qml:36
#, kde-format
msgid "Screens"
msgstr "Schermos"

#: src/ScreenChooserDialog.qml:39
#, kde-format
msgid "Windows"
msgstr "Fenestras"

#: src/ScreenChooserDialog.qml:126
#, kde-format
msgid "Allow restoring on future sessions"
msgstr "Permitte restabilir sur sessiones future"

#: src/screenshotdialog.cpp:115
#, kde-format
msgid "Full Screen"
msgstr "Schermo plen"

#: src/screenshotdialog.cpp:116
#, kde-format
msgid "Current Screen"
msgstr "Schermo currente"

#: src/screenshotdialog.cpp:117
#, kde-format
msgid "Active Window"
msgstr "Fenestra active"

#: src/ScreenshotDialog.qml:23
#, kde-format
msgid "Request Screenshot"
msgstr "Instantanee de schermo de requesta"

#: src/ScreenshotDialog.qml:30
#, kde-format
msgid "Capture Mode"
msgstr "Modo de captura"

#: src/ScreenshotDialog.qml:34
#, kde-format
msgid "Area:"
msgstr "Area:"

#: src/ScreenshotDialog.qml:39
#, kde-format
msgid "Delay:"
msgstr "Retardo:"

#: src/ScreenshotDialog.qml:44
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 secunda"
msgstr[1] "%1 secundas"

#: src/ScreenshotDialog.qml:52
#, kde-format
msgid "Content Options"
msgstr "Optiones de contento"

#: src/ScreenshotDialog.qml:56
#, kde-format
msgid "Include cursor pointer"
msgstr "Include punctator de cursor"

#: src/ScreenshotDialog.qml:61
#, kde-format
msgid "Include window borders"
msgstr "Include bordos de fenestra"

#: src/ScreenshotDialog.qml:74
#, kde-format
msgid "Save"
msgstr "Salveguarda"

#: src/ScreenshotDialog.qml:84
#, kde-format
msgid "Take"
msgstr "Prende"

#: src/session.cpp:169
#, kde-format
msgctxt "@action:inmenu stops screen/window sharing"
msgid "End"
msgstr "Fin"

#: src/session.cpp:255
#, kde-format
msgctxt ""
"SNI title that indicates there's a process remotely controlling the system"
msgid "Remote Desktop"
msgstr "Scriptorio remote"

#: src/session.cpp:271
#, kde-format
msgctxt "%1 number of screens, %2 the app that receives them"
msgid "Sharing contents to %2"
msgid_plural "%1 video streams to %2"
msgstr[0] "Compartir conmtentos a %2"
msgstr[1] "%1 fluxos de video a %2"

#: src/session.cpp:275
#, kde-format
msgctxt ""
"SNI title that indicates there's a process seeing our windows or screens"
msgid "Screen casting"
msgstr "Fusion de schermo"

#: src/session.cpp:441
#, kde-format
msgid ", "
msgstr ", "

#: src/userinfodialog.cpp:32
#, kde-format
msgid "Share Information"
msgstr "Comparti information"

#: src/userinfodialog.cpp:33
#, kde-format
msgid "Share your personal information with the requesting application?"
msgstr "Compartir tu information personal con le application requiriente?"

#: src/waylandintegration.cpp:260 src/waylandintegration.cpp:315
#, kde-format
msgid "Failed to start screencasting"
msgstr "Il falleva a initiar screencasting"

#: src/waylandintegration.cpp:619
#, kde-format
msgid "Remote desktop"
msgstr "Scriptorio remote"

#~ msgid ""
#~ "Laptop screen\n"
#~ "Model: %1"
#~ msgstr ""
#~ "Schermo de Laptop\n"
#~ "Modello: %1"

#~ msgid ""
#~ "Manufacturer: %1\n"
#~ "Model: %2"
#~ msgstr ""
#~ "Fabricante: %1\n"
#~ "Modello: %2"

#~ msgid "%1 is running in the background."
#~ msgstr "%1 es executante in le fundo."

#~ msgid "Find out more"
#~ msgstr "Trova altere"

#~ msgid "%1 is running in the background"
#~ msgstr "%1 es executante in le fundo"

#~ msgid ""
#~ "This might be for a legitimate reason, but the application has not "
#~ "provided one.\n"
#~ "\n"
#~ "Note that forcing an application to quit might cause data loss."
#~ msgstr ""
#~ "Isto poterea esser per un motivation legitime, ma le application non ha "
#~ "fornite alcun\n"
#~ "\n"
#~ "Nota que fortiar que un application quita pote causar perdita de datos."

#~ msgid "Force quit"
#~ msgstr "Fortia abandonar"

#~ msgid "Portal"
#~ msgstr "Portal"

#, fuzzy
#~| msgid "Full Screen"
#~ msgctxt "Will allow the app to see what's on the outputs"
#~ msgid ""
#~ " <li>Screens\n"
#~ "</li>"
#~ msgstr "Schermo plen"

#~ msgid "Select what to share with the requesting application"
#~ msgstr "Selige que compartir con le application requiriente"

#~ msgid "Select what to share with %1"
#~ msgstr "Selige que compartir con %1"

#~ msgid "Recording window \"%1\"..."
#~ msgstr "Registrante fenestra \"%1\"..."

#~ msgid "Recording screen \"%1\"..."
#~ msgstr "Registrante schermo \"%1\"..."

#~ msgid "Recording workspace..."
#~ msgstr "Registrante spatio de labor..."

#, fuzzy
#~| msgid "Recording window \"%1\"..."
#~ msgid "Recording virtual output '%1'..."
#~ msgstr "Registrante fenestra \"%1\"..."

#~ msgctxt "@action:inmenu stops screen/window recording"
#~ msgid "Stop Recording"
#~ msgstr "Stoppa Registrar"

#~ msgid "Dialog"
#~ msgstr "Dialogo"

#~ msgid "Allow access to:"
#~ msgstr "Permitte  accesso a :"

#~ msgid "Pointer"
#~ msgstr "Punctator"

#~ msgid "Keyboard"
#~ msgstr "Claviero"

#~ msgid "Touch screen"
#~ msgstr "Schermo a tacto"

#~ msgid "Press to cancel"
#~ msgstr "Pressa per cancellar"

#~ msgid "xdg-desktop-portals-kde"
#~ msgstr "xdg-desktop-portals-kde"

#~ msgid "Open with..."
#~ msgstr "Aperi con..."

#~ msgid "Icon"
#~ msgstr "Icone"

#~ msgid "TextLabel"
#~ msgstr "Etiquetta de Texto (TextLabel)"

#~ msgid "Full Screen (All Monitors)"
#~ msgstr "Schermo plen(Omne Monitores)"

#~ msgid "No Delay"
#~ msgstr "Necun retardo"

#~ msgid "Take screenshot"
#~ msgstr "Prende instantanee de schermo"

#~ msgid "Search"
#~ msgstr "Cerca"
